package com.neo4.parser.controller;

import com.neo4.parser.model.DataPoint;
import com.neo4.parser.model.Document;
import com.neo4.parser.repository.DataPointRepository;
import com.neo4.parser.repository.DocumentRepository;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.io.InputStream;
import java.net.URL;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class DocumentControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private DataPointRepository dataPointRepository;

    @Test
    public void create() throws Exception {
        //prepare
        InputStream is = new URL("http://www.orimi.com/pdf-test.pdf").openStream();
        MockMultipartFile body = new MockMultipartFile("file", is);
        //test + validate
        mvc.perform(multipart("/api/documents").file(body))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber());
    }

    @Test
    void getDataPoints() throws Exception {
        //prepare
        String url = "/api/documents/%d/data-points";
        Document document = new Document();
        document.setContent("test");
        document = documentRepository.save(document);
        DataPoint dataPoint = new DataPoint();
        dataPoint.setContent("test");
        dataPoint.setLabel("test");
        dataPoint = dataPointRepository.save(dataPoint);
        document.getDataPoints().add(dataPoint);
        documentRepository.save(document);
        //test + validate
        mvc.perform(get(String.format(url, document.getId())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(1)));
    }
}