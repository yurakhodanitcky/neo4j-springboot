package com.neo4.parser.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo4.parser.dto.DataPointUpdateDto;
import com.neo4.parser.model.DataPoint;
import com.neo4.parser.repository.DataPointRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class DataPointControllerTest {

    private final static String URL = "/api/data-points/%d/add-to-dataset";

    private final static DataPointUpdateDto BODY = new DataPointUpdateDto("test");

    @Autowired
    private DataPointRepository dataPointRepository;

    @Autowired
    private MockMvc mvc;

    @Test
    void update() throws Exception {
        //prepare
        DataPoint dataPoint = new DataPoint();
        dataPoint.setContent("test");
        dataPoint.setLabel("test");
        dataPoint = dataPointRepository.save(dataPoint);
        //test + validate
        mvc.perform(put(String.format(URL, dataPoint.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(BODY)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(dataPoint.getId()));
    }
}