package com.neo4.parser.service;

import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class PdfService {

    public String parse(MultipartFile file) throws IOException {

        InputStream inputstream = new ByteArrayInputStream(file.getBytes());

        PDFParser pdfparser = new PDFParser(new RandomAccessBufferedFileInputStream(inputstream));
        pdfparser.parse();

        PDDocument pdDoc = pdfparser.getPDDocument();
        PDFTextStripper pdfStripper = new PDFTextStripper();
        return pdfStripper.getText(pdDoc);
    }

}
