package com.neo4.parser.service;

import com.neo4.parser.dto.DataPointUpdateDto;
import com.neo4.parser.model.DataPoint;
import com.neo4.parser.model.Document;
import com.neo4.parser.repository.DataPointRepository;
import com.neo4.parser.repository.DocumentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
public class DataPointService {

    private final DataPointRepository dataPointRepository;
    private final DocumentRepository documentRepository;
    private final static String LABEL_DELIMITER = " ";

    @Transactional
    public void save(String content, Document document) {
        DataPoint point = new DataPoint();
        point.setLabel(content.substring(0, content.indexOf(LABEL_DELIMITER)));
        point.setContent(content);
        dataPointRepository.save(point);
        document.getDataPoints().add(point);
        documentRepository.save(document);
    }

    @Transactional
    public DataPoint update(long id, DataPointUpdateDto dto) {
        DataPoint dataPoint = dataPointRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("DataPoint with id " + id + " not found"));
        dataPoint.setContent(dataPoint.getContent() + "\n" + dto.getContent());
        return dataPointRepository.save(dataPoint);
    }
}
