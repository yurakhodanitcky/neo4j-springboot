package com.neo4.parser.service;

import com.neo4.parser.model.DataPoint;
import com.neo4.parser.model.Document;
import com.neo4.parser.repository.DocumentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;
import java.util.Set;

@Service
@AllArgsConstructor
public class DocumentService {

    private final DocumentRepository documentRepository;

    @Transactional
    public Document save(String data) {
        Document document = new Document();
        document.setContent(data);
        return documentRepository.save(document);
    }

    public Set<DataPoint> getDataPointsByDocumentId(long id) {
        Document document = documentRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Document with id " + id + " not found"));
        return document.getDataPoints();
    }

    @Transactional
    public void deleteById(long id) {
        documentRepository.deleteByIdWithDataPoints(id);
    }
}
