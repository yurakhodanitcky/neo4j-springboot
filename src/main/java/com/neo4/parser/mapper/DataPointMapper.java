package com.neo4.parser.mapper;


import com.neo4.parser.dto.DataPointDto;
import com.neo4.parser.model.DataPoint;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface DataPointMapper {

    DataPointDto toDto(DataPoint dataPoint);

    Set<DataPointDto> toDto(Set<DataPoint> dataPoints);

}
