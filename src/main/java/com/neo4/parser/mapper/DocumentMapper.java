package com.neo4.parser.mapper;


import com.neo4.parser.dto.DocumentDto;
import com.neo4.parser.model.Document;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DocumentMapper {

    DocumentDto toDto(Document dataPoint);

}
