package com.neo4.parser.repository;

import com.neo4.parser.model.Document;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

public interface DocumentRepository extends Neo4jRepository<Document, Long> {

    @Query("MATCH (d:Document)-[r]-(point) WHERE id(d) = $id DELETE d, r, point")
    void deleteByIdWithDataPoints(@Param("id") Long id);

}
