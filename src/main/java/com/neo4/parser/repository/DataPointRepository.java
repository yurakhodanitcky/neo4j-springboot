package com.neo4.parser.repository;

import com.neo4.parser.model.DataPoint;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface DataPointRepository extends Neo4jRepository<DataPoint, Long> {

}
