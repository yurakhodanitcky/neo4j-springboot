package com.neo4.parser.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
@Data
@NoArgsConstructor
public class Document {

    @Id
    @GeneratedValue
    private Long id;

    private String content;

    @Relationship(type = "DATAPOINT", direction = Relationship.UNDIRECTED)
    public Set<DataPoint> dataPoints = new HashSet<>();

}