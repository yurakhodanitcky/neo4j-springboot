package com.neo4.parser.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
@Data
@NoArgsConstructor
public class DataPoint {

    @Id
    @GeneratedValue
    private Long id;

    private String content;

    private String label;

}
