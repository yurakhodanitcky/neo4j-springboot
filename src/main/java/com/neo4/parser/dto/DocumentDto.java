package com.neo4.parser.dto;

import lombok.Data;

@Data
public class DocumentDto {

    private Long id;

    private String content;

}
