package com.neo4.parser.dto;

import lombok.Data;

@Data
public class DataPointDto {

    private Long id;

    private String content;

    private String label;

}
