package com.neo4.parser.controller;

import com.neo4.parser.dto.DataPointDto;
import com.neo4.parser.dto.DataPointUpdateDto;
import com.neo4.parser.mapper.DataPointMapper;
import com.neo4.parser.service.DataPointService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/data-points")
@Validated
@AllArgsConstructor
public class DataPointController {

    private final DataPointService dataPointService;
    private final DataPointMapper dataPointMapper;

    @PutMapping("{id}/add-to-dataset")
    public DataPointDto update(@RequestBody DataPointUpdateDto dto, @PathVariable long id) {
        return dataPointMapper.toDto(dataPointService.update(id, dto));
    }
}
