package com.neo4.parser.controller;

import com.neo4.parser.dto.DataPointDto;
import com.neo4.parser.dto.DocumentDto;
import com.neo4.parser.mapper.DataPointMapper;
import com.neo4.parser.mapper.DocumentMapper;
import com.neo4.parser.model.Document;
import com.neo4.parser.service.DataPointService;
import com.neo4.parser.service.DocumentService;
import com.neo4.parser.service.PdfService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "api/documents")
@Validated
@AllArgsConstructor
public class DocumentController {

    private final DocumentService documentService;
    private final DataPointService dataPointService;
    private final PdfService pdfService;
    private final DataPointMapper dataPointMapper;
    private final DocumentMapper documentMapper;
    private final static String PARAGRAPH_DELIMITER_REGEX = "\\n\\s+";

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public DocumentDto create(@RequestPart MultipartFile file) throws IOException {
        String content = pdfService.parse(file);
        Document document = documentService.save(content);
        List<String> dataPoints = Arrays.asList(content.split(PARAGRAPH_DELIMITER_REGEX));
        dataPoints.forEach(p -> {
            if (!p.isEmpty() && !p.trim().isEmpty())
                dataPointService.save(p, document);
        });
        return documentMapper.toDto(document);
    }

    @GetMapping("{id}/data-points")
    public Set<DataPointDto> getDataPoints(@PathVariable long id) {
        return dataPointMapper.toDto(documentService.getDataPointsByDocumentId(id));
    }

    @DeleteMapping("{id}")
    public void deleteById(@PathVariable long id) {
        documentService.deleteById(id);
    }
}
