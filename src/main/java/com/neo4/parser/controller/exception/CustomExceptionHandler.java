package com.neo4.parser.controller.exception;

import com.neo4.parser.pojo.CustomMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.util.NoSuchElementException;

@RestControllerAdvice
public class CustomExceptionHandler {

    private final String maxFileSize;
    private final String maxRequestSize;
    private final static String MAX_FILE_SIZE_MESSAGE = "The request was rejected because its size exceeds the specified maximum (file - %s, request - %s)";

    public CustomExceptionHandler(@Value(value = "${spring.servlet.multipart.max-file-size}") String maxFileSize,
                                  @Value(value = "${spring.servlet.multipart.max-request-size}") String maxRequestSize) {
        this.maxFileSize = maxFileSize;
        this.maxRequestSize = maxRequestSize;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            IllegalArgumentException.class,
    })
    protected CustomMessage handleIllegalArgumentException(IllegalArgumentException ex) {
        return new CustomMessage(ex.getMessage());
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler({
            NoSuchElementException.class,
    })
    protected CustomMessage handleNoSuchElementException(NoSuchElementException ex) {
        return new CustomMessage(ex.getMessage());
    }

    @ResponseStatus(value = HttpStatus.PAYLOAD_TOO_LARGE)
    @ExceptionHandler({
            MaxUploadSizeExceededException.class
    })
    protected CustomMessage handleMaxUploadSizeException(MaxUploadSizeExceededException ex) {
        return new CustomMessage(String.format(MAX_FILE_SIZE_MESSAGE, maxFileSize, maxRequestSize));
    }

}
